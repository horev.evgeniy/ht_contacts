package com.example.p2_hw1

import android.R
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Binder
import android.os.IBinder
import android.provider.ContactsContract
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat

class GetContactService : Service() {

    private val binder = LocalBinder()
    private val contactGetter = getContacts()

    inner class LocalBinder : Binder() {
        fun getService(): GetContactService = this@GetContactService
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

//    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//
//
//        stopSelf()
//
//        return START_NOT_STICKY
//    }
//
//    override fun onDestroy() {
//        Toast.makeText(this,"service done", Toast.LENGTH_SHORT).show()
//    }

    fun getContacts(): ArrayList<String> {
        val contacts = arrayListOf<String>()
        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val cursor = contentResolver.query(
            uri,
            null,
            null,
            null,
            null
        )
        cursor?.apply {
            val nameIndex = getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            val numIndex = getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            while (cursor.moveToNext()) {
                val name = cursor.getString(nameIndex)
                val number = cursor.getString(numIndex)
                contacts.add("$name $number")

            }
        }
        return contacts
    }
}