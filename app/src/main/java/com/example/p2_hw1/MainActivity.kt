package com.example.p2_hw1

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {

    private lateinit var mService: GetContactService
    private var mBound: Boolean = false
    private var contacts: ArrayList<String> = arrayListOf()
    private lateinit var contactList: ListView

    private val connection = object : ServiceConnection{
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as GetContactService.LocalBinder
            mService = binder.getService()
            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }
    override fun onStart() {
        super.onStart()
        Intent(this, GetContactService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }
    override fun onStop() {
        super.onStop()
        unbindService(connection)
        mBound = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            Intent(this, GetContactService::class.java).also { intent ->
                bindService(intent, connection, Context.BIND_AUTO_CREATE)
                Toast.makeText(this, "connected to service", Toast.LENGTH_SHORT).show()
                contacts = mService.getContacts()
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_CONTACTS),
                1
            )
            Intent(this, GetContactService::class.java).also { intent ->
                bindService(intent, connection, Context.BIND_AUTO_CREATE)
                Toast.makeText(this, "connected to service", Toast.LENGTH_SHORT).show()
                contacts = mService.getContacts()
            }
        }

        //val button = findViewById<Button>(R.id.button_getContacts)
        //button.setOnClickListener(getContactsButtonListener)

        contactList = findViewById<ListView>(R.id.lv_contacts)

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, contacts)
        contactList.adapter = adapter

    }

}